import React from 'react';
import { 
    Container,
    InputArea,
    CustomButtom,
    CustomButtomText,
    SignMessageButtom,
    SignMessageButtonText,
    SignMessageButtonTextBold 
} from './styles';
import { Text } from 'react-native';


import MedicineLogo from '../../assets/medicine.svg';
import SignInput from '../../components/SignInput';
import EmailIcon from '../../assets/email.svg';
import LockIcon from '../../assets/lock.svg'


export default() => {
    return (
        <Container>
            <MedicineLogo width="100%" height="160"/>

            <InputArea>
            

                <SignInput IconSvg={EmailIcon}
                placeholder="Digite seu email"/>
                <SignInput IconSvg={LockIcon}
                placeholder="Digite sua senha"/>
      
                <CustomButtom>
                    <CustomButtomText>LOGIN</CustomButtomText>
                </CustomButtom>

            </InputArea>

            <SignMessageButtom>
                <SignMessageButtonText>Ainda não possui uma conta?</SignMessageButtonText>
                <SignMessageButtonTextBold>Cadastre-se</SignMessageButtonTextBold>
            </SignMessageButtom>


        </Container>
    )
}