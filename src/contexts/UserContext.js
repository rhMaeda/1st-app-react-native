import React, { createContext, useReducer } from 'react';
import { initalState, initialState, UserReducer, userReducer } from '../reducers/UserReducer';

export const UserContext = createContext();

export default({children}) => {

    const [state, dispatch] = useReducer(UserReducer, initalState);

    return (
        <UserContext.Provider>
            {children}
        </UserContext.Provider>
    );
}